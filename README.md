## missi_phoneext4_cn-user 13 TKQ1.221114.001 V816.0.1.0.TKCCNXM release-keys
- Manufacturer: xiaomi
- Platform: holi
- Codename: veux
- Brand: qti
- Flavor: missi_phoneext4_cn-user
- Release Version: 13
- Kernel Version: 5.4.233
- Id: TKQ1.221114.001
- Incremental: V816.0.1.0.TKCCNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: zh-CN
- Screen Density: undefined
- Fingerprint: qti/veux/veux:13/TKQ1.221114.001/V816.0.1.0.TKCCNXM:user/release-keys
- OTA version: 
- Branch: missi_phoneext4_cn-user-13-TKQ1.221114.001-V816.0.1.0.TKCCNXM-release-keys
- Repo: qti_veux_dump
